﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;

namespace WpfApplication2
{
    public partial class MainWindow : Window
    {
        public ObservableCollection<Task> Tasks { get; set; }
        public ObservableCollection<Task> Task_in_proc { get; set; }
        public ObservableCollection<Task> Task_done { get; set; }
        public ICommand AddCommand { get; set; }
        public ICommand DelCommand { get; set; }
        public ICommand Del2Command { get; set; }
        public ICommand DelAllCommand { get; set; }
        public ICommand DelAll2Command { get; set; }
        public ICommand RCommand { get; set; }
        public ICommand R2Command { get; set; }
        public class Task : INotifyPropertyChanged
        {
            public event PropertyChangedEventHandler PropertyChanged;
            public string Data { get; set; }
            public string Task_one { get; set; }
        }
        class CustomCommand<T> : ICommand
        {
            private readonly Action<T> _action;

            public CustomCommand(Action<T> action)
            {
                _action = action;

            }
            public bool CanExecute(object parameter)
            {
                return true;
            }

            public void Execute(object parameter)
            {
                _action((T)parameter);

            }
            public event EventHandler CanExecuteChanged;
        }
        public MainWindow()
        {
            Tasks = new ObservableCollection<Task>();
            Task_in_proc = new ObservableCollection<Task>();
            Task_done = new ObservableCollection<Task>();

            AddCommand = new CustomCommand<Task>(Add);
            DelCommand = new CustomCommand<Task>(Del);
            DelAllCommand = new CustomCommand<Task>(DelAll);
            DelAll2Command = new CustomCommand<Task>(DelAll2);
            RCommand = new CustomCommand<Task>(Right);
            R2Command = new CustomCommand<Task>(Right2);

            DataContext = this;
            InitializeComponent();
        }

        private void Add(Task Nothing)
        {
            if (tasktext.Text == "")
            {
                MessageBox.Show("Пусто");
                return;
            }
            DB db = new DB();
            MySqlCommand command = new MySqlCommand("INSERT INTO tasks_now (task, timecreate) VALUES (@task_one, @data_task);", db.getConnection());

            string Data = Convert.ToString(DateTime.Now);
            command.Parameters.Add("@task_one", MySqlDbType.VarChar).Value = tasktext.Text;
            command.Parameters.Add("@data_task", MySqlDbType.VarChar).Value = Data;

            db.openConnection();
            if (command.ExecuteNonQuery() == 1)
                MessageBox.Show("Добавлено");
            else
                MessageBox.Show("He Добавлено");
            db.closeConnection();

            Task TasksItem = new Task();
            TasksItem.Task_one = tasktext.Text;
            TasksItem.Data = Data;

            Tasks.Add(TasksItem);
        }

        private void Del(Task task)
        {
            Tasks.Remove(task);
            Task_in_proc.Remove(task);
            Task_done.Remove(task);

            DB db = new DB();
            MySqlCommand command = new MySqlCommand("DELETE FROM tasks_now WHERE task = @task AND timecreate=@data_task", db.getConnection());

            command.Parameters.Add("@task", MySqlDbType.VarChar).Value = task.Task_one;
            command.Parameters.Add("@data_task", MySqlDbType.VarChar).Value = task.Data;

            db.openConnection();
            if (command.ExecuteNonQuery() == 1)
                MessageBox.Show("Удалено");
            else
                MessageBox.Show("He удалено");
            db.closeConnection();
        }

        private void Right(Task task)
        {
            Task_in_proc.Add(task);
            Tasks.Remove(task);
        }

        private void DelAll(Task Nothing)
        {
            int N = Tasks.Count();
            DB db = new DB();
            for (int i = N - 1; i >= 0; i--)
            {
                Task rez = Tasks[i];

                MySqlCommand command = new MySqlCommand("DELETE FROM tasks_now WHERE timecreate=@create ", db.getConnection());
                command.Parameters.Add("@create", MySqlDbType.VarChar).Value = rez.Data;

                db.openConnection();
                command.ExecuteNonQuery();
                db.closeConnection();
                Tasks.Remove(rez);
            }
        }

        private void DelAll2(Task Nothing)
        {
            int N = Task_done.Count();
            DB db = new DB();
            for (int i = N - 1; i >= 0; i--)
            {
                Task rez = Task_done[i];
                MySqlCommand command = new MySqlCommand("DELETE FROM tasks_now WHERE timecreate=@create ", db.getConnection());
                command.Parameters.Add("@create", MySqlDbType.VarChar).Value = rez.Data;

                db.openConnection();
                command.ExecuteNonQuery();
                db.closeConnection();
                Task_done.Remove(rez);
            }
        }

        private void Right2(Task task)
        {
            Task_done.Add(task);
            Task_in_proc.Remove(task);
        }
    }
}
